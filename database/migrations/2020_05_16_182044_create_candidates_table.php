<?php // קובץ שנוכל לבנות בו טבלה במסד נתונים באמצעות מייגרשן

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) { //יצירת טבלה דרך מייגרשן דרך פקודת הקרייאט
            $table->id();
            $table->string('name', 100);
            $table->string('email', 100);
            $table->timestamps();
        });
    }
    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates');
    }
}
