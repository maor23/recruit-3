<?php //נוסיף עמודה של ססטוס איידי בטבלת קאנדיידט כי חייב שיהיה עמודה מקשרת בין הטבלאות סטטוסים וקאנדיידיטס

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('candidates', function (Blueprint $table) {
            $table->bigInteger('status_id')->nullable()->after('email')->unsigned()->index; //נוסיף עמודה של ססטוס איידי לטבלת קאנדיידט כי חייב שיהיה עמודה מקשרת בין הטבלאות
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('candidates', function (Blueprint $table) {
            $table->dropColumn('status_id');
        });
    }
}
