<?php //נזריע את הקובץ דיפארטנמנטסידר

use Illuminate\Database\Seeder;
use Carbon\Carbon; 


class DepartmentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('departments')->insert([
            ['department_id' => 1, 'department_name' => "HR", 'created_at' => Carbon::now(), 'updated_at'=>Carbon::now()],
            ['department_id' => 2, 'department_name' => "Management", 'created_at' => Carbon::now(), 'updated_at'=>Carbon::now()],

        ]);
    }
}