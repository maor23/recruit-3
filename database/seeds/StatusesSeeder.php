<?php //נזריע את טבלת סטטוסים

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon; //הוספה שלי

class StatusesSeeder extends Seeder //לשים לב לשנות
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() //נזריע
    {
        DB::table('statuses')->insert([
            ['status_id' => 1, 'status_name' => "before interview", 'created_at' => Carbon::now(), 'updated_at'=>Carbon::now()],
            ['status_id' => 2, 'status_name' => "not fit", 'created_at' => Carbon::now(), 'updated_at'=>Carbon::now()],
            ['status_id' => 3, 'status_name' => "sent to manager", 'created_at' => Carbon::now(), 'updated_at'=>Carbon::now()],
            ['status_id' => 4, 'status_name' => "not fit professionally", 'created_at' => Carbon::now(), 'updated_at'=>Carbon::now()],
            ['status_id' => 5, 'status_name' => "accepted to work", 'created_at' => Carbon::now(), 'updated_at'=>Carbon::now()],
        ]);
    }
}