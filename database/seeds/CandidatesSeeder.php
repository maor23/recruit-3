<?php // נזריע את טבלת קאנדידייטס

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Carbon\Carbon; //הוספה שלנו לתרגיל בית 6

class CandidatesSeeder extends Seeder //לשים לב לשנות
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() 
    {
        DB::table('candidates')->insert([ //הזרעה של נתונים רנדומליים בטבלת קאנדידייטס
            'name' => Str::random(10),
            'email' => Str::random(5).'@gmail.com',
            'created_at' => Carbon::now(), //תרגיל בית 6
            'updated_at'=>Carbon::now()
        ]);
    }
}