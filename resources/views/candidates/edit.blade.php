@extends('layouts.app') <!--קובץ ויו --><!--נקשר לליאוט -->

@section('title', 'Edit Candidates')

@section('content')


                <!--הוספה של טופס -->
                <h1 class="text-center font-weight-bold text-black"> Edit candidate</h1> <!--בזכות הקומפקט שהוספנו בקונטרולר זה יודע לזהות את המשתנה קאדיידייט בשורות הבאות -->
                <form method = "post" action = "{{action('CandidatesController@update',$candidate->id)}}"class="needs-validation" novalidate> <!--נוסיף לאיזה מועמד אנחנו רוצים לבצע את העריכה --> <!--נוסיף פונקציית אפדייט ולא פונקציית סטור --> 
                @method('PATCH') <!--יש הסבר במחברת בהרצאה 8 על מה זה -->
                @csrf 
                <div>
                    <label for = "name">Candiadte name</label>
                    <input type = "text" name = "name" value = {{$candidate->name}}>  <!--נציג את השם של המועמד אותו אנחנו עורכים ספציפית ולא סתם שם של מומעד --> 
                </div>     
                <div>
                    <label for = "email">Candiadte email</label>
                    <input type = "text" name = "email" value = {{$candidate->email}}> <!--נציג את האימייל של המועמד אותו אנחנו עורכים ספציפית ולא סתם אימייל של מומעד --> 
                </div>     
                <div>
                    <input type = "submit" name = "submit" value = "Update candidate"> <!--נשנה לאפדייט במקום כרייאט --> 
                </div>                      
                </form>
@endsection
   
