@extends('layouts.app') <!--קובץ ויו --><!--נקשר לליאוט -->

@section('title', 'Create Candidate') <!--נקשר לטייטל בליאוט -->

@section('content') <!--נקשר לקונטנט לליאוט -->


                <!--הוספה של טופס -->
                    <h1 class="text-center font-weight-bold text-black"> Create candidate</h1>
                        <form method = "post" action ="{{action('CandidatesController@store')}}"class="needs-validation" novalidate> <!--נוסיף פונקציית סטור --> 
                            @csrf <!--לאבטחת מידע -->
                            <div>
                                <label for = "name">Candidate name</label>
                                <input type = "text" name = "name">
                            </div>    
                            <div>
                                <label for = "email">Candidate email</label>
                                <input type = "text" name = "email">
                            </div>
                            <div>
                                <input type = "submit" name = "submit" value = "Crete Candidate">
                            </div>

                        </form>
@endsection
