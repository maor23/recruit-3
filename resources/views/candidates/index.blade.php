@extends('layouts.app') <!--קובץ ויו --><!--נקשר לליאוט -->

@section('title', 'Candidates')

@section('content')

<!-- הרצאה 12 -->
@if(Session::has('notallowed'))
<div class ='alert alert-danger'>
    {{Session::get('notallowed')}}
</div>
@endif


<div><a href =  "{{url('/candidates/create')}}"> Add new candidate</a></div> <!--הוספה שלנו לתוספת קישור-->


            <!--הוספה שלנו שהמידע יוצג בצורת טבלה -->
                <h1 class="text-center font-weight-bold text-black"> List of candidates</h1>
                <table class="table table-striped">
                <table class="table table-hover">
                <thead class="thead-dark"> 
                    <tr> <!--הגדרה של העמודות בטבלה -->
                    <th>id</th><th>Name</th><th>Email</th><th>Status</th><th>Owner</th><th>Created</th><th>Updated</th><th>Edit</th><th>Delete</th>
                    </tr>
                    
                    <!-- the table data -->
                    @foreach($candidates as $candidate) <!--לולאה שמחליפה כל קאנדידייטס בדאנדידייט -->
                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$candidate->id}}</td><!--בזכות הקומפקט שהוספנו בקונטרולר זה מזהה את המשתמש-->
                            <td>{{$candidate->name}}</td>
                            <td>{{$candidate->email}}</td>
                            <td>  <!--נוסיף עמודה עם קוד מועתק מבווטסטראפ וקוד ששינינו כדי שיהי עמודה של כל הססטוסים תחת רשימה נפתחת--> 
                            <div class="dropdown">
                                    @if (null != App\Status::next($candidate->status_id)) <!--לא ני תן להשתמש ביוז בקובץ שהוא מסוג ויו, לכן בשביל להשתמש בפונקצייה נקסט שנמצאת במודל סטטוס, ככה נקרא לה --><!--רק אם יש סטטוסים מתאימים שייפתח דרופ דאון של רשימה, אם אין סטטוסים מתאימים שלא ייפתח בכלל דרופ דאון-->  
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><!--החלק של הפונקציונליות של הלחיצה בכפתור- בחירה מהרשימה -->
                                        @if(isset($candidate->status_id)) <!--שואלים את השאלה האם הוגדר לקאנדידייט סטטוס איידי -->
                                            {{$candidate->status->status_name}} <!--אם כן הוגדר אז כאן הריליישנשיפ שהגדרנו בא לידי ביטוי,נרצה שיופיע השם של הסטטוס שהוגדר לכל קנדידייט מסתתר פה ג'ויין -->
                                        @else <!--אם לא הוגדר לקאנדידייט סטטוס אידיי -->
                                            Define Status <!--למרות שכבר הגדרנו שהסטטוס איידי יהיה 1 כברירת מחדל לכן יופיע ישר ביפור אינטרוויו --><!--זה מה שיופיע כברירת מחדל -->
                                        @endif
                                    </button> 
                                    @else <!-- אם הגענו לסטטוס סופי שלא ניתן לעבור ממנו לסטטוס אחר אז שלא יופיע בכלל אופציה לפתיחת רשימת דרופ דאון שלא יבלבל את המשתמש שלא תיפתח סתם רשימה ריקה, לכן רק שיופיע השם של הסטטוטס הסופי-->
                                        {{$candidate->status->status_name}}
                                    @endif

                                    @if(App\Status::next($candidate->status_id) != null) <!--לא ני תן להשתמש ביוז בקובץ שהוא מסוג ויו, לכן בשביל להשתמש בפונקצייה נקסט שנמצאת במודל סטטוס, ככה נקרא לה --><!--רק אם יש סטטוסים מתאימים שייפתח דרופ דאון של רשימה, אם אין סטטוסים מתאימים שלא ייפתח בכלל דרופ דאון-->
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> <!--אם זה לא נאל וכן יש סטטוסים שצריכים להיפתח אז שייפתחו הסטטוסים המאושרים -->
                                        @foreach(App\Status::next($candidate->status_id) as $status) <!--נחליף את כל הקטע של הקריאה לפונקצייה נקסט מהמודל סטטוס, פשוט בססטוס  --><!--לולאה שמוודאה שייפתחו רק הסטטוסים הנכונים בעזרת הפונקצייה נקסט שרשמנו במודל סטטוס, פה עושים שאם הוגדר לקאנדידייטס דטטוס איידי אז תפעיל עליו את פונקציית נקסט ואם כל זה לא נאל זאת אומרת שיש סטטוסים מותרים אז תפתח רשימת דריל דאון עם כל הססטוסים המותרים -->
                                            <a class="dropdown-item" href="{{route('candidate.changestatus',[$candidate->id,$status->status_id])}}">{{$status->status_name}}</a><!-- הויו מעביר לראוט שמעביר לקונטרולר שמעביר למודל שמעביר לבסיס הנתונים ששם הם נשמרים, במערך שמים את הפרמטרים שהפונקצייה רוצה לקבל -->
                                        @endforeach
                                    </div>
                                    @endif
                                </div>
                            </td>
                            <td> <!-- נוסיף עמודה עם קוד מועתק מבווטסטראפ וקוד ששינינו כדי שיהי עמודה של כל היוזרים תחת רשימה נפתחת -->
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><!--החלק של הפונקציונליות של הלחיצה בכפתור- בחירה מהרשימה -->
                                        @if(isset($candidate->user_id))<!--שואלים את השאלה האם הוגדר לקאנדידייט יוזר איידי -->
                                            {{$candidate->owner->name}}<!--אם כן הוגדר אז כאן הריליישנשיפ שהגדרנו בא לידי ביטוי,נרצה שיופיע השם של היוזר שהוגדר לכל קנדידייט מסתתר פה ג'וין -->
                                        @else<!--אם לא הוגדר לקאנדידייט יוזר אידיי -->
                                            Assign owner<!--זה מה שיופיע כברירת מחדל -->
                                        @endif
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    @foreach($users as $user)<!--שתיפתח רשימה שתראה את כל היוזרים הקיימים --><!--לולאה שמחליפה כל יוזרס ביוזר -->
                                        <a class="dropdown-item" href="{{route('candidate.changeuser',[$candidate->id,$user->id])}}">{{$user->name}}</a><!-- הויו מעביר לראוט שמעביר לקונטרולר שמעביר למודל שמעביר לבסיס הנתונים ששם הם נשמרים, במערך שמים את הפרמטרים שהפונקצייה רוצה לקבל -->
                                    @endforeach
                                    </div>
                                </div>
                            </td>
                            <td>{{$candidate->created_at}}</td>
                            <td>{{$candidate->updated_at}}</td>
                            <td><a href = "{{route('candidates.edit',$candidate->id)}}">Edit</a></td> <!-- יצירת עמודה בטבלה שהיא קישור ל-אדייט תרגיל בית שבע -->
                            <td><a href = "{{route('candidate.delete',$candidate->id)}}">Delete</a></td> <!-- יצירת עמודה בטבלה שהיא קישור ל-דאליט השם של הראוט זה השם שאנחנו הגדרנו -->
                        </tr>
                    @endforeach
                    </tbody>
                </table>
@endsection
