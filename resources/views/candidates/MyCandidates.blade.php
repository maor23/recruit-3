<!-- קובץ של תרגיל בית 11 לא נשתמש בו יותר כי לרוני היה פתרון אחר -->
@extends('layouts.app') <!--קובץ ויו מועתק כמו האינדקס בלייד --><!--נקשר לליאוט -->

@section('title', 'MyCandidates')

@section('content')

<!-- תרגיל בית 11  נוריד את הקישור של הוספת מועמד חדש, כי מוסיפים אותו בדף של אינדקס נקודה בלייד-->
<!--<div><a href =  "{{url('/candidates/create')}}"> Add new candidate</a></div> --><!--הוספה שלנו לתוספת קישור-->


            <!--הוספה שלנו שהמידע יוצג בצורת טבלה -->
                <h1 class="text-center font-weight-bold text-black"> List of my candidates</h1>
                <table class="table table-striped">
                <table class="table table-hover">
                <thead class="thead-dark"> 
                    <tr> <!--הגדרה של העמודות בטבלה -->
                    <!--נוריד את העמודה של האונר בעמוד מיי קאנדיידיט כי יוזר שמגיע לעמוד הזה אז ברור שכל המועמדים הם שלו ושהוא האונר -->
                    <th>id</th><th>Name</th><th>Email</th><th>Status</th><!--<th>Owner</th>--><th>Created</th><th>Updated</th><th>Edit</th><th>Delete</th>
                    </tr>
                    
                    <!-- the table data -->
                    <!-- תרגיל בית 11 -->
                    @foreach(App\Candidate::userCandidate($userConnect->id) as $candidate) <!--נפנה לפונקציה יוזרקאנדידייט שהגדרנו במודל קאנדידייט, שרק המועמדים ששייכים ליוזר שמחובר יופיעו כ-אס קאנדידייט-->

                    </thead>
                    <tbody>
                        <tr>
                            <td>{{$candidate->id}}</td><!--בזכות הקומפקט שהוספנו בקונטרולר זה מזהה את המשתמש-->
                            <td>{{$candidate->name}}</td>
                            <td>{{$candidate->email}}</td>
                            <td>  <!--נוסיף עמודה עם קוד מועתק מבווטסטראפ וקוד ששינינו כדי שיהי עמודה של כל הססטוסים תחת רשימה נפתחת--> 
                            <div class="dropdown">
                                    @if (null != App\Status::next($candidate->status_id)) <!--לא ני תן להשתמש ביוז בקובץ שהוא מסוג ויו, לכן בשביל להשתמש בפונקצייה נקסט שנמצאת במודל סטטוס, ככה נקרא לה --><!--רק אם יש סטטוסים מתאימים שייפתח דרופ דאון של רשימה, אם אין סטטוסים מתאימים שלא ייפתח בכלל דרופ דאון-->  
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><!--החלק של הפונקציונליות של הלחיצה בכפתור- בחירה מהרשימה -->
                                        @if(isset($candidate->status_id)) <!--שואלים את השאלה האם הוגדר לקאנדידייט סטטוס איידי -->
                                            {{$candidate->status->status_name}} <!--אם כן הוגדר אז כאן הריליישנשיפ שהגדרנו בא לידי ביטוי,נרצה שיופיע השם של הסטטוס שהוגדר לכל קנדידייט מסתתר פה ג'ויין -->
                                        @else <!--אם לא הוגדר לקאנדידייט סטטוס אידיי -->
                                            Define Status <!--למרות שכבר הגדרנו שהסטטוס איידי יהיה 1 כברירת מחדל לכן יופיע ישר ביפור אינטרוויו --><!--זה מה שיופיע כברירת מחדל -->
                                        @endif
                                    </button> 
                                    @else <!-- אם הגענו לסטטוס סופי שלא ניתן לעבור ממנו לסטטוס אחר אז שלא יופיע בכלל אופציה לפתיחת רשימת דרופ דאון שלא יבלבל את המשתמש שלא תיפתח סתם רשימה ריקה, לכן רק שיופיע השם של הסטטוטס הסופי-->
                                        {{$candidate->status->status_name}}
                                    @endif

                                    @if(App\Status::next($candidate->status_id) != null) <!--לא ני תן להשתמש ביוז בקובץ שהוא מסוג ויו, לכן בשביל להשתמש בפונקצייה נקסט שנמצאת במודל סטטוס, ככה נקרא לה --><!--רק אם יש סטטוסים מתאימים שייפתח דרופ דאון של רשימה, אם אין סטטוסים מתאימים שלא ייפתח בכלל דרופ דאון-->
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"> <!--אם זה לא נאל וכן יש סטטוסים שצריכים להיפתח אז שייפתחו הסטטוסים המאושרים -->
                                        @foreach(App\Status::next($candidate->status_id) as $status) <!--נחליף את כל הקטע של הקריאה לפונקצייה נקסט מהמודל סטטוס, פשוט בססטוס  --><!--לולאה שמוודאה שייפתחו רק הסטטוסים הנכונים בעזרת הפונקצייה נקסט שרשמנו במודל סטטוס, פה עושים שאם הוגדר לקאנדידייטס דטטוס איידי אז תפעיל עליו את פונקציית נקסט ואם כל זה לא נאל זאת אומרת שיש סטטוסים מותרים אז תפתח רשימת דריל דאון עם כל הססטוסים המותרים -->
                                            <a class="dropdown-item" href="{{route('candidate.changestatus',[$candidate->id,$status->status_id])}}">{{$status->status_name}}</a><!-- הויו מעביר לראוט שמעביר לקונטרולר שמעביר למודל שמעביר לבסיס הנתונים ששם הם נשמרים, במערך שמים את הפרמטרים שהפונקצייה רוצה לקבל -->
                                        @endforeach
                                    </div>
                                    @endif
                                </div>
                            
                            </td>
                                                <!-- מחקתי את כל הקוד של היוזר - תרגיל בית 11-->
                            
                            
                            <td>{{$candidate->created_at}}</td>
                            <td>{{$candidate->updated_at}}</td>
                            <td><a href = "{{route('candidates.edit',$candidate->id)}}">Edit</a></td> <!-- יצירת עמודה בטבלה שהיא קישור ל-אדייט תרגיל בית שבע -->
                            <td><a href = "{{route('candidate.delete',$candidate->id)}}">Delete</a></td> <!-- יצירת עמודה בטבלה שהיא קישור ל-דאליט השם של הראוט זה השם שאנחנו הגדרנו -->
                        </tr>
                    </tbody>
                    @endforeach
                </table>
@endsection
