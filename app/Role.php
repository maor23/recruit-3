<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    
    //נגדיר קשר מאני טו מאני
    public function users(){
        return $this->belongsToMany('App\User','userroles');//מציינים בקשר כזה גם את השם של הטבלה המקשרת
    }
}
