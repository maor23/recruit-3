<?php //מודל שלארבל יצרה לבד

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB; //ספרייה שהוספנו על מנת לבנות שאילתה


class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'department_id' //!!!!!כמו שהוספתי את דיפארנטמנט, צריך להוסיף משתנים אם צריך
    ];


    // הוספה שלנו
    public function candidates(){ //ניצור פונקצייה שתגדיר קשרים בין הטבלאות בין שתי המודלים
        return $this -> hasMany('App\Candidate'); //נגדיר את השם של המודל אליו נרצה להתחבר הקאנדידט כיוון הסלאש הוא קריטי האס מאני זה אומר שמועמד שייך למשתמש, לכל יוזר יש כמה מועמדים
    
    }
 
    //תרגיל בית 10
    public function departments(){ 
    return $this->belongsTo('App\Department'/*,'department_id'*/); //לכל יוזר יש רק מחלקה אחת
    }

    
    //הרצאה 12
    //נגדיר קשר מאני טו מאני
    public function roles(){
        return $this->belongsToMany('App\Role','userroles');//מציינים בקשר כזה גם את השם של הטבלה המקשרת
    }


    public function isAdmin(){
        $roles = $this->roles; // נריץ שאילתה שלוקחת את היוזר הנוכחי, ומוצא את כל התפקידים שלו, רולס מחזיר רשימה  שיכולה לחזור עם 2 תפקידים, עם תפקיד 1, או בלי אף תפקיד
        if(!isset($roles)) return false; // אם זה חזר ריק
        foreach($roles as $role){ // מחזיר אדמין לכן תחזיר טרו
            if($role->name === 'admin') return true; //שלוש שווה כי הם גם שווים בערך וגם מאותו טייפ זה מגביר את אבטחת המידע
        }
        return false; // אחרת כנראה שזה מחזיר מנג'ר לכן תחזיר פאלס
    }


    public function isManager(){
        $roles = $this->roles; // נריץ שאילתה שלוקחת את היוזר הנוכחי ומוצא את כל התפקידים שלו, רולס מחזיר רשימה שיכולה לחזור עם 2 תפקידים, עם 1, או בלי אף תפקיד
        if(!isset($roles)) return false; // אם זה חזר ריק בלי אף תפקיד תחזיר פאלס
        foreach($roles as $role){ // מחזיר מנג'ר לכן תחזיר טרו
            if($role->name === 'manager') return true; //שלוש שווה כי הם גם שווים בערך וגם מאותו טייפ זה מגביר את אבטחת המידע
        }
        return false; // אחרת כנראה שזה מחזיר אמדין לכן תחזיר פאלס
    }
   


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


}
