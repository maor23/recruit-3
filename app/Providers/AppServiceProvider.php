<?php //קובץ שבו הוספנו קצת דברים כאשר העברנו את כל הקוד משרת מקומי לשרת מרוחק

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema; //הוספנו


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot() //הוספנו
    {
        Schema::defaultStringLength(191);
    }
}
